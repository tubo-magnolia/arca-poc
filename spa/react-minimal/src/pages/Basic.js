import React from 'react';
import { EditableArea } from '@magnolia/react-editor';

const Basic = props => {
  const { title } = props;

  return (
    <div className="Basic">
      <div className="hint">[Basic Page]</div>
      <h1>{title || props.metadata['@name']}</h1>

     
    </div>
  ) 
};

export default Basic;

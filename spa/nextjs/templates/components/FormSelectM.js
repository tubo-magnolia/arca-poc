import React from 'react';
import { EditableArea } from '@magnolia/react-editor';

function FormSelectM(props) {
  const { options, metadata } = props;
  return  (
    <label>
      {props.label} :
      <select value="please select an option">
      {options && <EditableArea content={options} parentTemplateId={metadata['mgnl:template']}/>
      }
      </select>
    </label>
    );
}

export default FormSelectM;

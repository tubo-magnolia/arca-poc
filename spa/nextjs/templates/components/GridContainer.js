
import React from 'react';
import { EditableArea } from '@magnolia/react-editor';
import Grid from '@material-ui/core/Grid';
// import { EditableArea } from '@magnolia/react-editor';

function GridContainer(props) {
  const { GridItemArea1, GridItemArea2, metadata } = props;

  return (
       <>
        <Grid />
        <Grid container spacing={3}>
        
        <Grid item xs={12} sm={8}>
            {GridItemArea1 && <EditableArea content={GridItemArea1} parentTemplateId={metadata['mgnl:template']}
            />  
        }
        </Grid>
        <Grid item xs={12} sm={4}>

            {GridItemArea2 && <EditableArea content={GridItemArea2} parentTemplateId={metadata['mgnl:template']}
                    />  
                }
        </Grid>
        
      </Grid>
    </>

  );
};
export default GridContainer;

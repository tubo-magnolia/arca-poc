import React from 'react';

function FormSelectOptionM(props) {
  return  (
    <option value="{props.value}">{props.label}</option>
    );
}

export default FormSelectOptionM;

import React from 'react';
import { EditableArea } from '@magnolia/react-editor';

function FormM(props) {
  const { form, metadata } = props;
  return  (
    <form>
      {form && <EditableArea content={form} parentTemplateId={metadata['mgnl:template']}/>
      }
    </form>
    );
}

export default FormM;

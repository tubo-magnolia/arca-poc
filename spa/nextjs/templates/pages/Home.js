import React from 'react';
import { EditableArea } from '@magnolia/react-editor';

function Home(props) {
  const { title, navigation, main, footer, metadata } = props;

  return (
    <div>
      <h1>{title}</h1>
      {navigation && <EditableArea content={navigation} parentTemplateId={metadata['mgnl:template']} />}
      {main && <EditableArea content={main} parentTemplateId={metadata['mgnl:template']} />}
      {footer && <EditableArea content={footer} parentTemplateId={metadata['mgnl:template']} />}
    </div>
  );
}

export default Home;

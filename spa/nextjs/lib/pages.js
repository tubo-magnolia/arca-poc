import Home from '../templates/pages/Home';
import Basic from '../templates/pages/Basic';
import Demo from '../templates/pages/Demo';
import Text from '../templates/components/Text';
import List from '../templates/components/List';
import Item from '../templates/components/Item';
import TourList from '../templates/components/TourList';
import GridContainer from '../templates/components/GridContainer';
import GridContainer111 from '../templates/components/GridContainer111';
import FormM from '../templates/components/FormM';
import FormSelectM from '../templates/components/FormSelectM';
import FormSelectOptionM from '../templates/components/FormSelectOptionM';



const nodeName = 'arca-home';

export const config = {
  componentMappings: {
    'spa-nextjs:pages/Home': Home,
    'spa-nextjs:pages/Basic': Basic,
    'spa-nextjs:pages/Demo': Demo,
    'spa-nextjs:components/Text': Text,
    'spa-nextjs:components/List': List,
    'spa-nextjs:components/Item': Item,
    'spa-nextjs:components/TourList': TourList,
    'spa-nextjs:components/GridContainer': GridContainer,
    'spa-nextjs:components/GridContainer111': GridContainer111,
    'spa-nextjs:components/FormM': FormM,
    'spa-nextjs:components/FormSelectM': FormSelectM,
    'spa-nextjs:components/FormSelectOptionM': FormSelectOptionM,
  },
};

export async function getPage(context) {
  let templateDefinitions = {};

  const pagesRes = await fetch(
    'http://localhost:8080/arca-poc-webapp/.rest/delivery/pages/' +
      nodeName +
      context.resolvedUrl.replace(new RegExp('(.*' + nodeName + '|.html)', 'g'), '')
  );
  console.info('http://localhost:8080/arca-poc-webapp/.rest/delivery/pages/' +
  nodeName);
  const page = await pagesRes.json();
  console.info(page);
  if (context.query.mgnlPreview === 'false') {
    const templateDefinitionsRes = await fetch(
      'http://localhost:8080/arca-poc-webapp/.rest/template-definitions/v1/' + page['mgnl:template']
    );

    templateDefinitions = await templateDefinitionsRes.json();
  }
 console.info('http://localhost:8080/arca-poc-webapp/.rest/template-definitions/v1/' + page['mgnl:template']);
  return { page, templateDefinitions };
}

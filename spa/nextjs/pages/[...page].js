import { EditablePage } from '@magnolia/react-editor';
import { getPage, config } from '../lib/pages';
import Head from 'next/head';


export async function getServerSideProps(context) {
  const page = await getPage(context);

  return {
    props: page,
  };
}

class App extends React.PureComponent {
  render() {
    const { page, templateDefinitions } = this.props;

    return (

      <div>        
        <Head>
          <title>This page has a title 🤔</title>
          <meta charSet="utf-8" />
          <meta name="viewport" content="initial-scale=1.0, width=device-width" />
          <meta name="aaa" content="aaaaa-scale=1.0, width=device-width" />

        </Head>
    {page && <EditablePage content={page} config={config} templateDefinitions={templateDefinitions} />}</div>
    );
  }
}

export default App;

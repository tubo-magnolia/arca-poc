import { getPage } from '../lib/pages';
import App from './[...page]';


export async function getServerSideProps(context) {
  const page = await getPage(context);

  return {
    props: page,
  };
}

export default App;

export const MY_SEO = {
  title: 'MyTitle',
  description: 'My description',
  openGraph: {
      type: 'website',
      url: 'My URL',
      title: 'MyTitle',
      description: 'My description',
      image: '...jpg',
  }
};

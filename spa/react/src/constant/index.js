
// const API_ENDPOINT = "http://localhost:8080/mpa-project-webapp/.rest/delivery/tours";
// const IMAGE_BASE = "http://localhost:8080";

const protocol = window.location.protocol.indexOf('https') > -1 ? 'https' : 'http';
const isLocalHost = (window.location.host.indexOf('0.0.0.0') > -1 || window.location.host.indexOf('localhost') > -1);
const HOST = isLocalHost ? `${protocol}://localhost:8080${process.env.REACT_APP_MGNL_BASE_AUTHOR}` : `${protocol}://${window.location.hostname}`;

export const IMAGE_BASE = isLocalHost ? 'http://localhost:8080' : '';

export const URL_ENDPOINT = HOST;

export const URL_TOUR = '/.rest/delivery/tours';

export const URL_EVENTS = '/.rest/events/'

export const SITE_BASE = process.env.REACT_APP_MGNL_BASE_AUTHOR;

export const APP_BASE = process.env.REACT_APP_MGNL_APP_BASE;
import { IMAGE_BASE } from "../constant";

export const formatDate = (date) => {
  return new Date(date).toLocaleDateString('en-US', {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  });
}

export const shortLinkHref = (link) => {
  return link.replace(process.env.REACT_APP_MGNL_APP_BASE_REPLACE, process.env.REACT_APP_MGNL_APP_BASE.replace('/', '')) + '.html';
}

export const showImage = (src) => {
  return `${IMAGE_BASE}${src}`;
}
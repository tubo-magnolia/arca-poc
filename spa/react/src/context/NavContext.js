import React, { createContext, useState, useEffect } from "react";
import { getAPIBase } from '../helpers/AppHelpers';

export const NavContext = createContext();

export const NavContextProvider = ({ children }) => {
  const [navItems, setNavItems] = useState([]);
  const [navMainItems, setNavMainItems] = useState([]);
  const [navTopItems, setNavTopItems] = useState([]);
  const [navFooterItems, setNavFooterItems] = useState([]);

  useEffect(() => {
    async function fetchNav() {
      const apiBase = getAPIBase();
      const url = apiBase + process.env.REACT_APP_MGNL_API_NAV + process.env.REACT_APP_MGNL_APP_BASE_REPLACE;
      const response = await fetch(url);
      const data = await response.json();
      console.log(data);
      let items = data['@nodes'].map((nodeName) => {
        return data[nodeName];
      });
      setNavItems([...items]);
      setNavMainItems([...items.filter((item) => item.navigation === 'main')]);
      setNavTopItems([...items.filter((item) => item.navigation === 'top')]);
      setNavFooterItems([...items.filter((item) => item.navigation === 'footer')]);
    }

    if (navItems.length < 1) {
      fetchNav();
    }

  }, [navItems]);

  return (
    <NavContext.Provider
      value={{
        navItems,
        navMainItems,
        navTopItems,
        navFooterItems,
      }}
    >
      {children}
    </NavContext.Provider>
  )
}
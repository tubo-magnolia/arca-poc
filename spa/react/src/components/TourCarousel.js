import React, { Fragment, useState, useEffect } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// import Swiper core and required modules
import SwiperCore, { Navigation, Pagination } from 'swiper/core';
// Import Swiper styles
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
import "swiper/components/navigation/navigation.min.css";

import axios from "axios";
import { URL_ENDPOINT, URL_TOUR, IMAGE_BASE } from "../constant";

SwiperCore.use([Navigation, Pagination]);

const TourCarousel = props => {
  const { title } = props;
  const [tours, setTours] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const res = await axios(`${URL_ENDPOINT}${URL_TOUR}`);
      setTours(res.data.results);
    };
    fetchData();
  }, []);

  return (
    <Fragment>
      <div className="tour-carousel">
        <h1>{title}</h1>
        {tours.length > 0 ? (
          <Swiper className="mySwiper"
            navigation={true}
            slidesPerView={3}
            spaceBetween={30}
            pagination={{
              "clickable": true
            }}
          >
            {tours.map(x => (
              <SwiperSlide>
                <img
                  src={IMAGE_BASE + x.image.renditions["480x360"].link}
                  className="card-img-top"
                  alt="..."
                />
                <div className="card-body">
                  <h5 className="card-title">{x.name}</h5>
                  <p className="card-text">{x.description}</p>
                </div>
              </SwiperSlide>
            ))}
          </Swiper>
        ) : (
          <div>Not found</div>
        )}
      </div>
    </Fragment>
  );
}

export default TourCarousel;
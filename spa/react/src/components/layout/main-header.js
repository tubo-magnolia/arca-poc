import React, { useContext, useState } from 'react';
// import { NavLink } from "react-router-dom";
import Navigation from "./navigation";
import HeaderTop from './headerTop';
import HeaderBot from './headerBot';
import { NavContext } from "../../context/NavContext";

const MainHeader = () => {
  const { navMainItems, navTopItems } = useContext(NavContext);
  const [isOpen, setIsOpen] = useState(false);

  return (
    <header className={`header container ${isOpen ? 'is-open' : ''}`}>
      <HeaderTop
        navTopItems={navTopItems}
      />
      <HeaderBot />
      <Navigation
        navMainItems={navMainItems}
        displayMenuMobile={isOpen => setIsOpen(isOpen)}
      />
    </header>
  );
}

export default MainHeader;
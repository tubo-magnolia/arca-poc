import React from 'react';
import { Fragment } from "react";
import MainHeader from "./main-header";
import Footer from './footer';

const Layout = ({ children }) => {
  return (
    <Fragment>
      <MainHeader />
      <main>{children}</main>
      <Footer />
    </Fragment>
  )
}

export default Layout;
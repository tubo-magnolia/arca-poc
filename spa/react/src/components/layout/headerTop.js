import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import { shortLinkHref } from "../../ultils";
import { showImage } from "../../ultils";
import { SITE_BASE } from "../../constant";
const HeaderTop = ({ navTopItems }) => {

  return (
    <Fragment>
      <div className="py-2 bg-light border-bottom">
        <div className="container d-flex flex-wrap">
          <ul className="nav nav--top me-auto align-items-center">
            {navTopItems && navTopItems.map((item) => (
              <li className="dropdown nav-item" key={item['@id']}>
                <NavLink
                  to={shortLinkHref(item['@path'])}
                  className="nav-link link-dark"
                > {item.title}
                </NavLink>

                {item['@nodes'].length > 0 && (
                  <Fragment>
                    <span className="dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"
                    ><span className="caret"></span>
                    </span>
                    <ul className="dropdown-menu">
                      {item['@nodes'].map((i) => (
                        <li className="nav-item" key={item[i]['@id']}>
                          <NavLink
                            to={shortLinkHref(item[i]['@path'])}
                            className="nav-link link-dark"
                          >
                            {item[i].title}
                          </NavLink>
                        </li>
                      ))}
                    </ul>
                  </Fragment>
                )}
              </li>
            ))}
          </ul>

          <ul className="nav">
            <li className="nav-item">
              <NavLink to={'/'} className="px-2">
                <img
                  src={showImage(`${SITE_BASE}/.resources/templating-foundation/webresources/images/logo-sg-govt.png`)}
                  alt=""
                />
              </NavLink>
            </li>
          </ul>
        </div>
      </div>

    </Fragment>
  );
}

export default HeaderTop;
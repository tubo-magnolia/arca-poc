import React, { Fragment } from "react";
import { NavLink } from "react-router-dom";
import { showImage } from "../../ultils";
import { SITE_BASE, APP_BASE } from "../../constant";
const HeaderBot = () => {

  return (
    <Fragment>
      <div className="d-flex flex-wrap align-items-center py-4">
        <NavLink
          className="d-flex align-items-center mb-3 mb-lg-0 me-lg-auto text-dark text-decoration-none"
          to={`${SITE_BASE}${APP_BASE}`}
        >
          <img src={showImage(`${SITE_BASE}/.resources/templating-foundation/webresources/images/logo-mpa.png`)} alt=""></img>
          <span className="fs-6 px-4">Maritime and Port Authority of Singapore</span>
        </NavLink>

        <div className="col-12 col-lg-4 header__controller">
          <div className="font-controller">
            <div className="font-controller__label">Font Size</div>
            <div className="font-controller__btns">
              <span className="font-controller__btn minus">-</span>
              <span className="font-controller__btn increment">+</span>
            </div>
          </div>
          <form className="input-group rounded search-form" action={`${SITE_BASE}${APP_BASE}/searchResults.html`}>
            <input type="search" className="form-control rounded" name="queryStr" placeholder="Search" aria-label="Search" aria-describedby="search-addon" />
            <button type="submit" className="input-group-text border-0" id="search-addon">
              <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-search" viewBox="0 0 16 16">
                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"></path>
              </svg>
            </button>
          </form>
        </div>
      </div>
    </Fragment>
  );
}

export default HeaderBot;
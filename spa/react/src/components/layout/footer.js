import React, { useContext } from 'react';
import { NavContext } from "../../context/NavContext";
import { shortLinkHref } from '../../ultils';

const Footer = () => {
  const { navFooterItems } = useContext(NavContext);

  return (
    <footer className="footer">
      <div className="container">
        <div className="row">
          <div className="col col-md-4">
            <div className="col-lg-12">
              <div className="text-section">
                <h2 className="footer__title">Contact Us</h2>
                <p>Phone: 1234567</p>
                <p>Email: example@gmail.com</p>
              </div>
              <div className="pt-4"></div>

              <div className="inline socials">
                <h4>Socials</h4>

                <ul>
                  <li>
                    <a href="https://twitter.com/" target="_blank" rel="noopener noreferrer" className="link">
                      <span className="fa fa-twitter-square" aria-hidden="true"></span>
                    </a>
                  </li>
                  <li>
                    <a href="https://www.facebook.com/" target="_blank" rel="noopener noreferrer" className="link">
                      <span className="fa fa-facebook-square" aria-hidden="true"></span>
                    </a>
                  </li>
                </ul>
              </div>
              <div className="pt-4"></div>
            </div>
          </div>
          <div className="col col-md-3">
            <div className="col-lg-12">
              <div className="text-section">
                <h2 className="footer__title">Location</h2>
                <p>Office 1</p>
                <p>Address detail 1</p>
                <p>City , Nation, Code</p>
                <p>&nbsp;</p>
                <p>Office 2</p>
                <p>Address detail 2</p>
                <p>City , Nation, Code</p>
              </div>
              <div className="pt-4"></div>
            </div>
          </div>
          <div className="col col-md-5 px-4">
            <div className="footer__title">MENU</div>

            <ul className="footer__nav">
              {navFooterItems && navFooterItems.map((item) => (
                <li className="nav-item" key={item['@id']}>
                  <a
                    href={shortLinkHref(item['@path'])}
                    className="nav-link link-dark"
                  >
                    {item.title}
                  </a>
                </li>
              ))}

            </ul>
          </div>
        </div>
        <div className="row justify-content-around border-top">
          <div className="col col-md-6">
            © 2021 Maritime and Port Authority of Singapore
          </div>
          <div className="col col-md-6 text-end">Last updated 2 Jan 2021</div>
        </div>
      </div>
    </footer>

  );
}

export default Footer;
import React, { useState } from 'react';
import HambergerIcon from '../../icons/hamberger-icon';
import { shortLinkHref } from '../../ultils';
const Navigation = (props) => {

  const { navMainItems, displayMenuMobile } = props;
  const [active, setActive] = useState(false);

  function handleClick() {
    setActive(!active);
    displayMenuMobile(!active);
  }

  return (
    <div className="nav-main-wrapper">
      <HambergerIcon
        handleClick={handleClick}
        active={active}
      />
      <ul className="nav nav--main">
        {navMainItems && navMainItems.map((item) => {
          return (
            <li className="nav-item" key={item['@id']}>
              <a
                href={shortLinkHref(item['@path'])}
                className="nav-link link-dark"
              >
                {item.navigationTitle || item.title}</a>
            </li>
          )
        })}
      </ul>
    </div>
  );
}

export default Navigation;
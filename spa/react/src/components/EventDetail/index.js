import React, { useEffect, useState } from 'react';
import EventDetailImage from './EventDetailImage';
import EventDetailContent from './EventDetailContent';

import { URL_ENDPOINT, URL_EVENTS } from "../../constant";
import axios from 'axios';
import { useRouteMatch } from 'react-router';
import Loading from '../Loading';
import { EditorContextHelper } from '@magnolia/react-editor';

const EventDetail = () => {
  const router = useRouteMatch();
  const { eventId } = router.params;
  const [event, setEvent] = useState(null);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);

  let URL = `${URL_ENDPOINT}${URL_EVENTS}?@name=${eventId}`;
  if (EditorContextHelper.inEditor()) {
    URL = `${URL_ENDPOINT}${URL_EVENTS}?limit=1`;
  }

  useEffect(() => {
    const fetchEvent = async () => {
      setLoading(true);
      setError(false);
      try {
        const result = await axios.get(URL);
        console.log('result', result.data.results);
        const [data] = result.data.results;
        console.log(data.name);
        setEvent(data);
      } catch (err) {
        setError(true);
      }
      setLoading(false);
    }
    // Call the API
    fetchEvent();
  }, [eventId]);

  return (

    <div className="container">
      {loading && <Loading isFullScreen={true} />}
      {error && <div>Error</div>}
      {event &&
        <EventDetailImage
          image={event.imageLink.renditions.large.link}
          name={event.name}
        />
      }

      {event &&
        <EventDetailContent
          location={event.location}
          date={event.date}
          description={event.description}
        />
      }
    </div>
  );
}

export default EventDetail;
import React from 'react';

const SideBar2 = () => {
  return (
    <div className="sidebar-box">
      <h3 className="heading">Related Posts</h3>
      <ul className="categories">
        <li>
          <a href="/#">Public Outreach <span>(12)</span></a>
        </li>
        <li>
          <a href="/#">Maritime Careers <span>(22)</span></a>
        </li>
        <li>
          <a href="/#">Education and Scholarships <span>(37)</span></a>
        </li>
        <li>
          <a href="/#">Green Efforts <span>(42)</span></a>
        </li>
        <li>
          <a href="/#">Port Statistics <span>(14)</span></a>
        </li>
      </ul>
    </div>
  );
}

export default SideBar2;
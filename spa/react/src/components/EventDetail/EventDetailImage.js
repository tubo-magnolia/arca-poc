import React from 'react';
import { showImage } from '../../ultils';
const EventDetailImage = (props) => {
  const { image, name } = props;

  return (
    <div className="p-4 p-md-5 mb-4 event-banner rounded"
      style={{ backgroundImage: "url(" + showImage(image) + ")" }}
    >
      <h1 className="display-4 fst-italic">{name}</h1>
    </div>
  );
}

export default EventDetailImage;
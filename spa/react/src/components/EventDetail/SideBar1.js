import React from 'react';

const SideBar1 = () => {
  return (
    <div className="sidebar-box">
      <h3 className="heading">What Maritime Singapore offers</h3>
      <div className="post-entry-sidebar">
        <ul>
          <li>
            <a href="/#">
              <img src="https://picsum.photos/90" alt="placeholder" className="me-4" />
              <div className="text">
                <h4>Global Connectivity</h4>
                <div className="post-meta">
                  <span className="mr-2">March 15, 2021 </span>
                </div>
              </div>
            </a>
          </li>
          <li>
            <a href="/#">
              <img src="https://picsum.photos/90" alt="placeholder" className="me-4" />
              <div className="text">
                <h4>Pro-Business Environment</h4>
                <div className="post-meta">
                  <span className="mr-2">March 15, 2021 </span>
                </div>
              </div>
            </a>
          </li>
          <li>
            <a href="/#">
              <img src="https://picsum.photos/90" alt="placeholder" className="me-4" />
              <div className="text">
                <h4>Global Maritime Services</h4>
                <div className="post-meta">
                  <span className="mr-2">March 15, 2021 </span>
                </div>
              </div>
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default SideBar1;
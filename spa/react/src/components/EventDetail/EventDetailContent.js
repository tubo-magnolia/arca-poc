import React from 'react';
import LocationIcon from '../../icons/location-icon';
import DateIcon from '../../icons/date-icon';
import { formatDate } from '../../ultils';
import EventDetailSideBar from './EventDetailSideBar';
const EventDetailContent = (props) => {

  const { location, date, description } = props;

  return (
    <div className="row g-5">
      <div className="col-12 col-lg-8 main-content">
        <ul className="d-flex list-unstyled ms-auto mb-5">
          <li className="d-flex align-items-center me-3">
            <LocationIcon />
            <small>{location}</small>
          </li>
          <li className="d-flex align-items-center">
            <DateIcon />
            <small>{formatDate(date)}</small>
          </li>
        </ul>
        <article className="blog-post" dangerouslySetInnerHTML={{ __html: description }}>
        </article>
        <nav className="blog-pagination" aria-label="Pagination">
          <a className="btn btn-outline-primary" href="/#">Older</a>
          <a className="btn btn-outline-secondary disabled" href="/#" tabIndex="-1" aria-disabled="true">Newer</a>
        </nav>
      </div>
      <EventDetailSideBar />
    </div>
  );
}

export default EventDetailContent;

import React from 'react';
import SideBar1 from './SideBar1';
import SideBar2 from './SideBar2';
import SideBar3 from './SideBar3';

const EventDetailSideBar = () => {
  return (
    <div className="col-12 col-lg-4 sidebar">
      <SideBar1 />
      <SideBar2 />
      <SideBar3 />
    </div>
  );
}

export default EventDetailSideBar;
import React from "react";
import { Spinner } from 'react-bootstrap';

const Loading = ({ isFullScreen }) => {
  return (
    <div className={`text-center loading${isFullScreen ? ' full' : ''}`}>
      <Spinner animation="border" />
    </div>
  );
}

export default Loading;
import React, { useState, useEffect } from "react";
import "./TourList.scss";

const API_ENDPOINT =
  "http://localhost:8080/mpa-project-webapp/.rest/delivery/tours";
const IMAGE_BASE = "http://localhost:8080";

export default ({ headline }) => {
  const [tours, setTours] = useState([]);

  useEffect(() => {
    async function fetchTourList() {
      let response = await fetch(API_ENDPOINT);
      let data = await response.json();
      setTours(data.results);
    }
    fetchTourList();
  }, []);

  return (
    <div className="tour-list">
      <h1>{headline}</h1>
      {tours.length > 0 ? (
        <div className="tour-list-cards">
          {tours.map(tour => (
            <div class="card">
              <img
                src={IMAGE_BASE + tour.image.renditions["480x360"].link}
                class="card-img-top"
                alt="..."
              />
              <div class="card-body">
                <h5 class="card-title">{tour.name}</h5>
                <p class="card-text">{tour.description}</p>
              </div>
            </div>
          ))}
        </div>
      ) : (
        <h2>No Tours found</h2>
      )}
    </div>
  );
};
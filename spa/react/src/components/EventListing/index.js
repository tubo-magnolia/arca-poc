import React, { Fragment, useEffect, useState } from "react";
import axios from "axios";
import EventItem from "./EventItem";
import Loading from "../Loading";
import { URL_ENDPOINT, URL_EVENTS, IMAGE_BASE } from "../../constant";

const EventListing = (props) => {
  const { title } = props;
  const [events, setEvents] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true);
      try {
        const res = await axios(`${URL_ENDPOINT}${URL_EVENTS}events-group/@nodes`);
        setEvents(res.data);
      } catch (err) {
        setLoading(false);
      }
      setLoading(false);
    };
    fetchData();
  }, []);

  return (
    <Fragment>
      {loading && <Loading />}
      <div className="container">
        <div className="events-list">
          <h2 className="pb-2 border-bottom">{title || 'Event Listing'}</h2>
          {events.length > 0 ? (
            <div className="row row-cols-1 row-cols-lg-3 align-items-stretch g-4 py-5">
              {events.map((event) => (
                <EventItem
                  key={event['@id']}
                  title={event.name}
                  location={event.location}
                  image={IMAGE_BASE + event.imageLink.renditions.small.link}
                  date={event.date}
                  link={'/react-event-detail/' + event['@name']}
                />
              ))}
            </div>
          ) : (
            <div className="center">No List found</div>
          )}

        </div>
      </div>
    </Fragment>
  );
}

export default EventListing;
import React from "react";
import { NavLink } from 'react-router-dom';
import LocationIcon from '../../icons/location-icon';
import DateIcon from '../../icons/date-icon';


const EventItem = (props) => {
  const { title, location, image, date, link } = props;

  const humanReadableDate = new Date(date).toLocaleDateString('en-US', {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  });

  return (
    <div className="col">
      <NavLink
        to={link}
        className="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg"
        style={{ backgroundImage: "url(" + image + ")" }}
      >
        <div className="d-flex flex-column text-white text-shadow-1">
          <h4 className="display-6 fw-bold card-title">{title}</h4>
          <ul className="d-flex list-unstyled ms-auto">
            <li className="d-flex align-items-center me-3">
              <LocationIcon />
              <small>{location}</small>
            </li>
            <li className="d-flex align-items-center">
              <DateIcon />
              <small>{humanReadableDate}</small>
            </li>
          </ul>
        </div>
      </NavLink>
    </div>
  );
}

export default EventItem;
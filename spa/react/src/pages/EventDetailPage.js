import React from 'react';
import Layout from '../components/layout/layout';
import { EditableArea } from '@magnolia/react-editor';

const EventDetailPage = props => {
  console.log('propsss: ', props);
  const { main } = props;

  return (
    <Layout>
      {main && <EditableArea className="Area" content={main} />}
    </Layout>
  )
};

export default EventDetailPage;

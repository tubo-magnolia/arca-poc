import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import PageLoader from './helpers/PageLoader';
import './App.css';

import { getRouterBasename } from './helpers/AppHelpers';

import { NavContextProvider } from './context/NavContext';

function App() {

  return (
    <Router basename={getRouterBasename()}>
      <Switch>
        <NavContextProvider>
          <Route
            path="/react-event-detail/:eventId"
            render={(props) => <PageLoader {...props} pagePath="/react-event-detail" />}
          />
          <Route
            path="/" component={PageLoader}
          />

        </NavContextProvider>
      </Switch>
    </Router>
  );
}

export default App;

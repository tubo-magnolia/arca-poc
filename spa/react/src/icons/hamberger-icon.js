import React from "react";

function HambergerIcon(props) {
  const { active, handleClick } = props;
  return (
    <div className={`hamburger-icon ${active ? 'active' : ''}`} onClick={handleClick}>
      <span className="hamburger-icon__line hamburger-icon__line--1"></span>
      <span className="hamburger-icon__line hamburger-icon__line--2"></span>
      <span className="hamburger-icon__line hamburger-icon__line--3"></span>
    </div>
  );
}

export default HambergerIcon